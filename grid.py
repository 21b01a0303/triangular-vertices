import sys
from functools import *
from itertools import *
from math import *
class inf:
    def _init_(self):
        self.v = "inf"
    def _eq_(self, other):
        return type(other) == type(self)
    def _str_(self):
        return "infinity"
class Grid:
    def _init_(self):
        arr = []
        x = 200
        c = 1
        self.x = x
        for i in range(x):
            k = []
            for j in range(i + 1):
                k.append(c)
                c += 1
            arr.append(k)
        self.arr = arr
    def position(self,y):
        iy = 1
        while y > (iy ** 2 + iy) / 2:
            iy += 1
        cy = y - int((iy ** 2 + iy) / 2) + iy - 1
        iy -= 1
        return (iy,cy)
    def returnSlope(self,z, y):
        p1 = self.position(z)
        p2 = self.position(y)
        if p1[0]==p2[0]:
            m1 = inf()
        else:
            m1 = (p1[1] - p2[1]) / (p1[0] - p2[0])
        return m1
    def inSameLine(self, x, y):
        p1 = self.position(x)
        p2 = self.position(y)
        if p1[0]==p2[0]:
            return True
        if p2[0] - p2[1] == p1[0] - p1[1]:
            return True
        if p1[1] == p2[1] or x == y:
            return True
        return False
    def areInSameLine(self,a):
        x = a[0]
        y = a[1]
        z = a[2]
        m1 = self.returnSlope(x, y)
        m2 = self.returnSlope(y, z)
        return m1 == m2
grid = Grid()
mul = lambda x, y: x * y
t = 1
points = [int(i) for i in sys.argv[1:len(sys.argv)]]
length = len(set(points))
print("ln ",length)
if length < 3:
    print(points, " is not an acceptable figure")
elif length == 3:
    if grid.isTriangle(points):
        print(points, " is a triangle")
    else:
        print(points, " is not an acceptable figure")
elif length == 4:
    points = sorted(points)
    if grid.sameline4(points):
        print(points," is not an acceptable figure")
        exit()

if grid.parallelogram(points):
        print(points, " is a parallelogram")
    else:
        k = list(combinations(points, 3))
        b2 = []
        for h in points:
            for i in range(len(k)):
                if h not in k[i]:
                    b2.append(h)
        c = -1
        for i in k:
             if grid.areInSameLine(i):
                c = i
                break
        if c!=-1:
            if not grid.sameline4(points) and grid.isTriangle([k[c[0]],k[c[2]],b2[c]]):
                print(points, " is A Triangle ")
        else:
            print(points, " is not an acceptable figure")
elif length == 5:
    print(points)
    points = sorted(points)
    tri = [[0, 3, 4], [0, 2, 4], [0, 1, 4]]
    tri2 = [[points[i[0]], points[i[1]], points[i[2]]] for i in tri]
    plg = [[0, 2, 3, 4], [0, 1, 3, 4], [0, 1, 2, 4]]
    plg2 = [[points[i[0]], points[i[1]], points[i[2]], points[i[3]]] for i in plg]
    j = [[0,1,2],[1,2,3],[2,3,4]]
    j2 = [[points[i[0]], points[i[1]], points[i[2]]] for i in j]
    k = -1
    for i in range(3):
        if grid.areInSameLine(j2[i]):
            print(j2[i])
            k = i
            break
    plg2 = [[points[i[0]], points[i[1]], points[i[2]], points[i[3]]] for i in plg]
    t = False
    if k!=-1:
        if grid.parallelogram(plg2[k]):
            t = True
            print(points, " is Parallelogram",plg2[k])
            exit()
    else:
        print(points," is not an acceptable figure 1")
        t = 1
        exit()
    k = False
    for i in tri2:
        if grid.isTriangle(i):
            k = True
            break
     for i in plg2:
            if grid.parallelogram(i) and c:
                print(points, " is parallelogram")
                break
        tri = [[1, 4, 6], [1, 3, 6]]
        tri2 = [[points[i[0]], points[i[1]], points[i[2]]] for i in tri]
        k = [[1, 2, 4], [1, 3, 6], [4, 5, 6]]
        k2 = [[points[i[0]], points[i[1]], points[i[2]]] for i in tri]
        f = True
        for i in k2:
            if not grid.areInSameLine(i):
                f = False
                break
        g = False
        for i in plg2:
            if grid.isTriangle(i) and f:
                print(points, " is Triangle")
                g = True
                break
        if not g:
            print(points," is not a acceptable figure")
else:
    print("Input Error")
